# Instruction-Manual-DCS

## BIOS settings

1. Press **Del** on boot screen to enter BIOS Setup
2. set **Power On** on AC Power Loss **( Boot > On AC/Power Loss > Power On )**
3. enable **Quiet Boot** **( Boot > Quiet Boot > Enabled )**
4. set administrator Password using password that's been agreed before **( Security > BIOS Password > set Password )**
5. Press **F10** to Save Configuration and reboot

## OS Instalation

1. Download lubuntu 20.04 [here](https://lubuntu.me/downloads/)
2. Create Bootable USB with [rufus](https://rufus.ie/en/) or [ventoy](https://www.ventoy.net/en/index.html)
3. Plug USB Flash Disk that contains **Lubuntu 20.04 LTS** installation
4. Press **F11** on boot screen and choose plugged in USB Flash Disk
5. Choose **Start Lubuntu** and wait until desktop displayed
6. Make sure IPC is connected to internet through **Ethernet LAN 1**
7. Click **Install Lubuntu 20.04 LTS** then follow the instructions in installation wizard
8. install with username **stechoq**
9. set password using password that's been agreed before
10. After installation is finished, when prompted to remove Flash Disk, remove it then press **ENTER**

Use [this guide](https://www.tecmint.com/install-lubuntu) if need picture instalation

## OS Setting

1. Start the device and wait enter desktop
2. Open terminal or press **Ctrl+Alt+T**
3. Clone dcs-preparation repository
   ```
   git clone https://gitlab.com/IlhamM3/dcs-preparation.git
   ```
4. after clone finished
   ```
   cd dcs-preparation && ./configure.sh "branch"
   ```
   **change branch according to branch you want to clone**

## VPN Setting

1. Start the device and wait enter desktop
2. Open terminal or press **Ctrl+Alt+T**
3. Clone dcs-Electron repository
   ```
   git clone https://gitlab.com/IlhamM3/DCS-Electron.git
   ```

## Change Hostname

1. Open scripts folder on dcs-preparation

   ```
   cd dcs-preparation/scripts
   ```

2. Change mode hostname scripts
   ```
   sudo chmod +x 13-change-hostname
   ```
3. Run hostname scripts

   ```
   sudo ./13-change-hostname "NEW-HOSTNAME"
   ```

   **Change NEW-HOSTNAME e.g. Setting**

   **Dont use space character**
