#!/bin/bash

ASSETS_DIR=$1
LOGFILE=$2


#=========== FUNCTIONS ==============
required_question () {
    local ANSWER
    while true
    do
        read -rp "$1" ANSWER
        if [ ! -z "${ANSWER}" ]; then
            break
        fi
    done

    eval "$2=$ANSWER"
}

optional_question () {
    local ANSWER
    read -rp "$1" ANSWER

    if [ -z "${ANSWER}" ]; then
        ANSWER=$2
    fi

    eval "$3=$ANSWER"
}

check_git_url () {
    local SSH_URL=""
    local HTTPS_URL=""

    if [ ! -z "$1" ]; then
	if [[ "$1" == *"https://"* ]]; then

            HTTPS_URL="$1"
            SSH_URL="$(sed 's#https://gitlab.com/#git@gitlab.com:#g' <<< $HTTPS_URL)"

        elif [[ "$1" == *"git@gitlab.com"* ]]; then

            SSH_URL="$1"
            HTTPS_URL="$(sed 's#git@gitlab.com:#https://gitlab.com/#g' <<< $SSH_URL)"

        fi
	
    fi
    
    eval "$2=$SSH_URL"
    eval "$3=$HTTPS_URL"
}

upper () {
    local RESULT
    RESULT="$(echo \"$1\" | tr a-z A-Z)"
    eval "$2=$RESULT"
}

#============ DEFINE =================
GIT_SSH_URL=""
GIT_HTTPS_URL=""
GIT_BRANCH="master"

API_BASE_URL=""
API_USERNAME=""
API_PASSWORD=""

MACHINE_ID=1

WINDOW_WIDTH=1280
WINDOW_HEIGHT=800
WINDOW_FULLSCREEN=true

SKIP_ALL='n'

AUTORELOAD=true

USE_SOCKET=true
SOCKET_PATH='0.0.0.0'
SOCKET_PORT=3000

USE_RFID=true
RFID_IP="192.168.253.200"
RFID_PORT=502
RFID_ID=255
RFID_TIMEOUT=1000

USE_PLC=true
PLC_IP="192.168.88.255"
PLC_CHANNEL=2
PLC_PORTUDP=4545


#============ QUESTION ================

printf "Please answer required form below !\n"
printf "\nThis question is about your git.\n"
required_question "What is URL (https:// or git:) for your repository ? " GIT_URL_RESULT
check_git_url $GIT_URL_RESULT GIT_SSH_URL GIT_HTTPS_URL

required_question "What is branch's name ? " GIT_BRANCH

printf "\nNext question will be about your main configuration.\n"
required_question "What is domain's URL for your BackEnd (include https://) ? " API_BASE_URL
required_question "What is username for sign-in on BackEnd ? " API_USERNAME
required_question "What is password for sign-in on BackEnd ? " API_PASSWORD
required_question "What is machine ID for this DCS ? " MACHINE_ID

printf "\nAll required form was already answered.\n"
printf "\nNext question will be about optional configuration.\n"
read -n 1 -rp "If you want to skip it, then press (y/Y)..." SKIP_ALL

upper $SKIP_ALL SKIP_ALL
if [ "$SKIP_ALL" !=  "Y" ]; then
    printf "\n\n"
    optional_question "Electron window's width ? (default: ${WINDOW_WIDTH}) " $WINDOW_WIDTH WINDOW_WIDTH
    optional_question "Electron window's height ? (default: ${WINDOW_HEIGHT}) " $WINDOW_HEIGHT WINDOW_HEIGHT
    optional_question "Is it fullscreen ? (default: ${WINDOW_FULLSCREEN}) " $WINDOW_FULLSCREEN WINDOW_FULLSCREEN

    optional_question "Is it auto-reload ? (default: ${AUTORELOAD}) " $AUTORELOAD AUTORELOAD

    optional_question "Do you want to skip socket communication ? (default: ${USE_SOCKET}) " $USE_SOCKET USE_SOCKET
    if [ "$USE_SOCKET" != "true" ]; then
        optional_question "What is socket's path ? (default: ${SOCKET_PATH}) " $SOCKET_PATH SOCKET_PATH
        optional_question "What is socket's port ? (default: ${SOCKET_PORT}) " $SOCKET_PORT SOCKET_PORT
    fi

    optional_question "Do you want to skip socket RFID OMRON ? (default: ${USE_RFID}) " $USE_RFID USE_RFID
    if [ "$USE_RFID" != "true" ]; then
        optional_question "What is RFID's IP Address ? (default: ${RFID_IP}) " $RFID_IP RFID_IP
        optional_question "What is RFID's port ? (default: ${RFID_PORT}) " $RFID_PORT RFID_PORT
        optional_question "What is RFID's ID ? (default: ${RFID_ID}) " $RFID_ID RFID_ID
        optional_question "How long timeout time (milliseconds) ? (default: ${RFID_TIMEOUT}) " $RFID_TIMEOUT RFID_TIMEOUT
    fi

    optional_question "Do you want to skip PLC integration ? (default: ${USE_PLC}) " $USE_PLC USE_PLC
    if [ "$USE_PLC" != "true" ]; then
        optional_question "What is PLC's IP Address ? (default: ${PLC_IP}) " $PLC_IP PLC_IP
        optional_question "What is PLC's UDP Port ? (default: ${PLC_PORTUDP}) " $PLC_PORTUDP PLC_PORTUDP
        optional_question "What is PLC's channel that will be used ? (default: ${PLC_CHANNEL}) " $PLC_CHANNEL PLC_CHANNEL
    fi
fi

sed -i "s@__TARGET_API_BASE_URL__@$API_BASE_URL@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_API_USERNAME__@$API_USERNAME@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_API_PASSWORD__@$API_PASSWORD@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_MACHINE_ID__@$MACHINE_ID@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_WINDOW_WIDTH__@$WINDOW_WIDTH@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_WINDOW_HEIGHT__@$WINDOW_HEIGHT@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_WINDOW_FULLSCREEN__@$WINDOW_FULLSCREEN@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_AUTORELOAD__@$AUTORELOAD@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_SOCKET_PATH__@$SOCKET_PATH@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_SOCKET_PORT__@$SOCKET_PORT@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_RFID_IP__@$RFID_IP@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_RFID_PORT__@$RFID_PORT@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_RFID_ID__@$RFID_ID@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_RFID_TIMEOUT__@$RFID_TIMEOUT@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_PLC_IP__@$PLC_IP@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_PLC_CHANNEL__@$PLC_CHANNEL@g" ${ASSETS_DIR}/app.config.js
sed -i "s@__TARGET_PLC_PORTUDP__@$PLC_PORTUDP@g" ${ASSETS_DIR}/app.config.js

#store some value to log
printf "GIT_SSH_URL=\"$GIT_SSH_URL\"\nGIT_HTTPS_URL=\"$GIT_HTTPS_URL\"\nGIT_BRANCH=\"$GIT_BRANCH\"" | tee ${LOGFILE} > /dev/null
