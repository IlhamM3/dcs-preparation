#!/bin/sh

#update and install
printf "\n===== Running System Update =====\n"

#change source.list to local repo
isJammy=$(lsb_release -r | grep -o "22.04" | tail -1)

if [[-z isJammy]]; then
    printf "deb http://archive.ubuntu.com/ubuntu/ focal main restricted\n
    deb http://archive.ubuntu.com/ubuntu/ focal-updates main restricted\n
    deb http://archive.ubuntu.com/ubuntu/ focal universe\n
    deb http://archive.ubuntu.com/ubuntu/ focal-updates universe\n
    deb http://archive.ubuntu.com/ubuntu/ focal multiverse\n
    deb http://archive.ubuntu.com/ubuntu/ focal-updates multiverse\n
    deb http://archive.ubuntu.com/ubuntu/ focal-backports main restricted universe multiverse\n
    deb http://archive.ubuntu.com/ubuntu/ focal-security main restricted\n
    deb http://archive.ubuntu.com/ubuntu/ focal-security universe\n
    deb http://archive.ubuntu.com/ubuntu/ focal-security multiverse\n" | sudo tee /etc/apt/sources.list > /dev/null
else
    printf "deb http://archive.ubuntu.com/ubuntu/ jammy main restricted\n
    deb http://archive.ubuntu.com/ubuntu/ jammy-updates main restricted\n
    deb http://archive.ubuntu.com/ubuntu/ jammy universe\n
    deb http://archive.ubuntu.com/ubuntu/ jammy-updates universe\n
    deb http://archive.ubuntu.com/ubuntu/ jammy multiverse\n
    deb http://archive.ubuntu.com/ubuntu/ jammy-updates multiverse\n
    deb http://archive.ubuntu.com/ubuntu/ jammy-backports main restricted universe multiverse\n
    deb http://archive.ubuntu.com/ubuntu/ jammy-security main restricted\n
    deb http://archive.ubuntu.com/ubuntu/ jammy-security universe\n
    deb http://archive.ubuntu.com/ubuntu/ jammy-security multiverse\n" | sudo tee /etc/apt/sources.list > /dev/null

fi

#remove unused software
printf "Removing software that's not needed for operation\n"
sudo  firefox 2048-qt featherpad vlc k3b libreoffice-base-core libreoffice-common libreoffice-draw libreoffice-help-common libreoffice-impress libreoffice-qt5 libreoffice-style-colibre libreoffice-writer libreoffice-calc libreoffice-core libreoffice-gtk3 libreoffice-help-en-us libreoffice-math libreoffice-style-breeze libreoffice-style-tango transmission-common transmission-qt trojita qpdfview snapd

sudo apt update -y

# if [[ "$doUpgrade" == "y" || "$doUpgrade" == "Y" ]]; then
# 	printf "===== Running System Upgrade =====\n"
# 	printf "%s\n" "$OSPassword" | sudo --stdin apt upgrade -y
# fi

sudo apt install --fix-missing -y
sudo apt install -y nano git nodejs npm x11vnc openssh-server net-tools nmap dkms

#sometimes installation is broken, try running it again with --fix-missing first
sudo apt install --fix-missing -y
sudo apt install -y nano git nodejs npm x11vnc openssh-server net-tools nmap dkms


#remove unused libraries
printf "==== Running autoremove =====\n"
sudo apt clean -y
sudo apt autoremove -y


