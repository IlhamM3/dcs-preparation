#!/bin/sh

# https://github.com/cilynx/rtl88x2bu
: <<'END'
git clone https://github.com/cilynx/rtl88x2bu.git /opt/rtl88x2bu
chmod +x /opt/rtl88x2bu/*.sh
 /opt/rtl88x2bu/deploy.sh
END

if [ "$(id -u)" -ne "0" ]; then
	echo "Please run as root"
	exit 1
fi

# https://github.com/RinCat/RTL88x2BU-Linux-Driver
git clone "https://github.com/RinCat/RTL88x2BU-Linux-Driver.git" /usr/src/rtl88x2bu-git
sed -i 's/PACKAGE_VERSION="@PKGVER@"/PACKAGE_VERSION="git"/g' /usr/src/rtl88x2bu-git/dkms.conf
dkms add -m rtl88x2bu -v git
dkms autoinstall

