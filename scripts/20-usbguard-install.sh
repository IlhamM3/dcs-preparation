#!/bin/bash

printf "===== USBGUARD Installation =====\n"
printf "CAUTION : Your Keyboard + Mouse might be unusable after usbguard installation"
read -n 1 -p "Install usbguard (Y/n)? " installUsbguard

if [[ "${installUsbguard,,}" != "n" || -z $installUsbguard ]]; then
	#usb guard installation
	printf "\nInstalling usbguard...\n"
	sudo apt install -y usbguard

	#usb guard config
	lsusb | grep -Po "[a-f0-9]+\:[a-f0-9]+" | while read line; do echo "allow id $line"; done | sudo tee /etc/usbguard/rules.conf

	#disabling global keyboard shortcut
	mkdir -p ~/.notused && mv -t ~/.notused /etc/xdg/autostart/lxqt-globalkeyshortcuts.desktop
else
	echo "ABORTED..."
fi
