#!/bin/bash

if [ "$(id -u)" -ne "0" ]; then
	echo "Please run as root"
	exit 1
fi


if [[ -z ${1} ]]; then
	_LOGGEDUSER=${USER}
else
	_LOGGEDUSER=${1}
fi

curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | BINDIR=/usr/bin sh
chmod +x /usr/bin/arduino-cli

arduino-cli completion bash > arduino-cli.sh
mv arduino-cli.sh /etc/bash_completion.d/

sudo -H -u ${_LOGGEDUSER} bash -c "arduino-cli core install arduino:avr"

_ARDUINODIR="/opt/arduino-dcs"
_ARDUINOGITSSH="git@gitlab.com:stechoq/dcs/ypti/arduino-dcs.git"
_ARDUINOGITHTTPS="https://gitlab.com/stechoq/dcs/ypti/arduino-dcs.git"


# Cloning and uploading arduino program
echo "=== Cloning arduino git as ${_LOGGEDUSER} to ${_ARDUINODIR} ==="
sudo -H -u ${_LOGGEDUSER} bash -c "git clone ${_ARDUINOGITSSH} ${_ARDUINODIR}"
sudo -H -u ${_LOGGEDUSER} bash -c "cd ${_ARDUINODIR} && make all"

# change git URL to https. Using '#' as delimiter because URLs contain '/' characters
sed -i "s#${_ARDUINOGITSSH}#${_ARDUINOGITHTTPS}#g" ${_ARDUINODIR}/.git/config
