#!/bin/sh

# ============== SET STATIC IP =====================

#~ #use random number from 2...102 instead of asking user to input
#~ ipAddressInput=$(( ( RANDOM % 100 ) + 2 ))

#~ #apply static ip via nmcli. put these lines at the end to avoid disconnecting internet connection
#~ #mod by 100 to make sure ip is not above 100
#~ ipAddressInput=$(($ipAddressInput%100))

#~ #if IP is 0 or 1, change to 2 as 1 and 0 is reserved
#~ if [[ $ipAddressInput = 1 || $ipAddressInput = 0 ]]; then
	#~ ipAddressInput=$(($ipAddressInput + 2))
#~ fi

#~ #assign IP address to be set
#~ ipClass="192.168.88"
#~ ipAddress="$ipClass.$ipAddressInput"
#~ ipGW="$ipClass.1"

#~ #get active device name
#~ #in case multiple active device is listed select the last one only,
#~ #because the procedure is to connect PLC to the last LAN socket, which is enp3s0
#~ ethernetDev=$(nmcli device status | grep -v 'disconnected' | grep 'connected' | grep 'ethernet' | grep -o '^\S*' | tail -1)

#~ #get connection name
#~ conName=$(nmcli -t -f DEVICE,NAME c show --active | grep "$ethernetDev\:" | sed "s/$ethernetDev\://g")

#~ #set IP to static and manual
#~ printf "===== Set Static IP to device \"$ethernetDev\" \"$conName\" =====\n"
#~ nmcli con mod "$conName" ip4 "$ipAddress"/24 gw4 $ipGW ipv4.dns "$ipGW 1.1.1.1 8.8.8.8" ipv4.method "manual"
#~ nmcli con up "$conName"

 #assign IP address RFID to be set on wired conn 2 (center)
 ipClass2="192.168.253" #this class just example
 ipAddress2="$ipClass2.1"

 ipClass3="192.168.252" #this class just example
 ipAddress3="$ipClass3.101"

 printf "===== Set Static IP to device \"$ipAddress\" =====\n"

 sudo nmcli con mod "Wired connection 2" ip4 "$ipAddress2/24" ipv4.method "manual" ipv4.never-default yes ipv4.ignore-auto-routes yes ipv4.ignore-auto-dns yes
sudo nmcli con up "Wired connection 2"

 sudo nmcli con mod "Wired connection 3" ip4 "$ipAddress3/24" ipv4.method "manual" ipv4.never-default yes ipv4.ignore-auto-routes yes ipv4.ignore-auto-dns yes
sudo nmcli con up "Wired connection 3"

#  ipGW="$ipClass.1"

#~ #get active device name
#~ #in case multiple active device is listed select the last one only,
#~ #because the procedure is to connect PLC to the last LAN socket, which is enp3s0
#~ ethernetDev=$(nmcli device status | grep -v 'disconnected' | grep 'connected' | grep 'ethernet' | grep -o '^\S*' | tail -1)

#~ #get connection name
#~ conName=$(nmcli -t -f DEVICE,NAME c show --active | grep "$ethernetDev\:" | sed "s/$ethernetDev\://g")

#~ #set IP to static and manual
#~ printf "===== Set Static IP to device \"$ethernetDev\" \"$conName\" =====\n"
#~ nmcli con mod "$conName" ip4 "$ipAddress"/24 gw4 $ipGW ipv4.dns "$ipGW 1.1.1.1 8.8.8.8" ipv4.method "manual"
#~ nmcli con up "$conName"

# ============== END OF SET STATIC IP =====================
printf "IP_ADDRESS=\"$ipAddress\"\n" | tee ${LOGFILE} > /dev/null
