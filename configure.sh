#!/bin/bash

#save script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ASSETS_DIR=${DIR}"/assets"

#git directory
GIT_DIR="/opt/sugity-dcs-device"

#log file
LOGFILE="/home/$USER/.sugity_dcsConfigured"

#shortcut to start x11vnc
X11VNC_SHORTCUT="/opt/x11vnc.sh"

if [ -f "${LOGFILE}" ]; then
    rerun=1
fi

#check if installed OS is Lubuntu 20.04 or 22.04
isLubuntu=$(cat /var/log/installer/media-info | grep -io "Lubuntu")
isFocal=$(lsb_release -r | grep -o "20.04" | tail -1)
isJammy=$(lsb_release -r | grep -o "22.04" | tail -1)

if [[ -z $isLubuntu || ( -z $isFocal && -z $isJammy ) ]]; then
    echo -e "\e[95m"
    echo "=================================================================================="
    echo "Must Be Lubuntu 20.04 or 22.04. Re-Install this computer with Lubuntu 20.04 Focal fossa or 22.04 Jammy Jellyfish"
    echo ""
    echo "Current Installation :"
    echo "$(cat /var/log/installer/media-info | grep -i 'buntu')"
    echo "$(lsb_release -r)"
    echo "=================================================================================="
    echo -e "\e[49m"
    exit 1
fi

#check internet connection, if no connection available exit then shutdown
wget -q --spider https://gitlab.com

if [ $? -eq 0 ]; then
    echo "Internet Connection Available"
else
    echo "No Internet Connection Available"
    echo "Connect to Internet then restart this device"
    sleep 10
    poweroff
    exit 1
fi

#ask user to input password for sudo
read -rs -p "Input Password for [$USER]: " OSPassword
sudo -k
#repeat until password is correct
while ! echo "$OSPassword" | sudo -S -v &> /dev/null; do
    echo "Wrong password!"
    read -rs -p "Input Password for [$USER]: " OSPassword
    sudo -k
done

#run entry prompt
${DIR}/scripts/110-entry-prompt.sh ${ASSETS_DIR} ${LOGFILE}

printf "\n\n"
GIT_SSH_URL="$(cat ${LOGFILE} | grep "GIT_SSH_URL"| head -1 | sed -E "s/(GIT_SSH_URL=\")|(\")//g")"
GIT_HTTPS_URL="$(cat ${LOGFILE} | grep "GIT_HTTPS_URL"| head -1 | sed -E "s/(GIT_HTTPS_URL=\")|(\")//g")"
GIT_BRANCH="$(cat ${LOGFILE} | grep "GIT_BRANCH"| head -1 | sed -E "s/(GIT_BRANCH=\")|(\")//g")"

#add to group dialout
printf "===== Adding user '$USER' to group 'dialout' =====\n"
sudo usermod -a -G dialout $USER

#added nopasswd to sudoers
sudoersFile=/etc/sudoers
sudoersText="$USER     ALL=(ALL) NOPASSWD:ALL"
printf "$sudoersText\n" | sudo tee -a $sudoersFile > /dev/null


#remove unused modules and install required modules
${DIR}/scripts/10-apt.sh


#install nodejs v14 lts
printf "===== Installing nodejs v14 lts =====\n"
sudo npm i n -g
sudo n v14

#~ clear

if [ -z $rerun ]; then
	printf "===== Configuring Lubuntu Settings =====\n"

	#disabling some autostarts by moving them to another directory instead of removing them
	mkdir -p ~/.notused \
	&& sudo mv -t ~/.notused \
	/etc/xdg/autostart/lxqt-panel.desktop \
	/etc/xdg/autostart/lxqt-notifications.desktop \
	/etc/xdg/autostart/lxqt-powermanagement.desktop \
	/etc/xdg/autostart/lxqt-runner.desktop \
	/etc/xdg/autostart/lxqt-qlipper-autostart.desktop \
	/etc/xdg/autostart/upg-notifier-autostart.desktop \
	/etc/xdg/autostart/snap-userd-autostart.desktop

	#copy wallpaper image
	sudo cp ${DIR}/img/wallpaper.png /usr/share/lubuntu/wallpapers/sugity_dcs-wallpaper.png

	#copy xscreensaver profile, to prevent screen turned off
	cp -rf ${DIR}/profile/.xscreensaver /home/$USER/.xscreensaver

	#overwrite pcmanfm-qt lxqt profile
	cp -rf ${DIR}/profile/settings.conf /etc/xdg/xdg-Lubuntu/pcmanfm-qt/lxqt/settings.conf

	#set number of workspace to 1 by overwriting openbox config with preset value
	sudo cp -rf ${DIR}/profile/lxqt-rc.xml /etc/xdg/xdg-Lubuntu/openbox/lxqt-rc.xml

	#remove desktop icon
	sudo sed -i 's/Wallpaper\=.*/Wallpaper\=\/usr\/share\/lubuntu\/wallpapers\/sugity_dcs-wallpaper\.png/g' /etc/xdg/xdg-Lubuntu/pcmanfm-qt/lxqt/settings.conf
	sudo sed -i 's/WallpaperMode\=.*/WallpaperMode\=fit/g' /etc/xdg/xdg-Lubuntu/pcmanfm-qt/lxqt/settings.conf
	sudo sed -i 's/DesktopShortcuts\=.*/DesktopShortcuts\=\@Invalid\(\)/g' /etc/xdg/xdg-Lubuntu/pcmanfm-qt/lxqt/settings.conf

	#copy config to user's folder
	cp -rf /etc/xdg/xdg-Lubuntu/openbox ~/.config
	cp -rf /etc/xdg/xdg-Lubuntu/pcmanfm-qt ~/.config

	rm -rf ~/Desktop/*.desktop

	#Create shortcut to x11vnc
	printf "x11vnc -display :0 -auth ~/.Xauthority" | sudo tee ${X11VNC_SHORTCUT} > /dev/null
	sudo chown $USER:$USER ${X11VNC_SHORTCUT}
	sudo chmod 755 ${X11VNC_SHORTCUT}

	#~ clear

	#create Autologin user in sddm desktop manager
	printf "Setting Autologin for [$USER]\n"
	printf "[Autologin]\nUser=$USER\nSession=Lubuntu\n\n" | sudo tee /etc/sddm.conf > /dev/null

	#changing to custom plymouth by editing default.plymouth
	printf "===== Setting Plymouth =====\n"
	sudo cp -rf ${DIR}/plymouth/themes/sugity_dcs /usr/share/plymouth/themes/sugity_dcs
	sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/sugity_dcs/sugity_dcs.plymouth 100
	cat /usr/share/plymouth/themes/sugity_dcs/sugity_dcs.plymouth | sudo tee /usr/share/plymouth/themes/default.plymouth > /dev/null
	sudo update-initramfs -u

	#change grub timeout on recordfail
	sudo sed -i '/GRUB_RECORDFAIL_TIMEOUT/d' /etc/default/grub
	printf "GRUB_RECORDFAIL_TIMEOUT=0\n" | sudo tee -a /etc/default/grub > /dev/null && sudo update-grub

	#change ssh config
	#change port from default 22 to 65432
	sudo sed -i 's/#Port.*/Port 65432/g' /etc/ssh/sshd_config

	#change password auth to no
	#~ sudo sed -i 's/#PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config

	#~ clear

	#install RFID DRIVER
	sudo ${DIR}/scripts/21-disable-nfc-pn533-kmod.sh

	#install wifi rtl88x2bu driver
	sudo ${DIR}/scripts/30-rtl88x2bu-wifi.sh	
fi

#copy default authorized pubkey
mkdir -p ~/.ssh
cp -rf ${DIR}/profile/authorized_keys ~/.ssh/authorized_keys
#copy ssh key to clone gitlab repo
cp -rf ${DIR}/profile/rsa/sugity_dcs_rsa ~/.ssh/id_rsa
cp -rf ${DIR}/profile/rsa/sugity_dcs_rsa.pub ~/.ssh/id_rsa.pub
#set permission
chmod 700 ~/.ssh
chmod 600 ~/.ssh/*

#add gitlab.com to known_host
ssh-keygen -F gitlab.com || ssh-keyscan gitlab.com >>~/.ssh/known_hosts

#restart ssh service
sudo service sshd restart

#clone repo and checkout used branch
if [[ -z "${GIT_BRANCH}" ]]; then
	GIT_BRANCH="master"
fi

printf "==== Initializing sugity dcs device and checkout branch '$GIT_BRANCH' =====\n"

#if git folder already exist, delete it
if [[ -d ${GIT_DIR} ]]; then
	sudo rm -rf ${GIT_DIR}
fi

#Create dircetory for git clone
sudo mkdir -p ${GIT_DIR}
sudo chown $USER:$USER ${GIT_DIR}
sudo chmod 755 ${GIT_DIR}

GIT_URL=$GIT_SSH_URL

git clone -b $GIT_BRANCH $GIT_URL ${GIT_DIR}

#set shell files as executable
cd ${GIT_DIR} && chmod +x ${GIT_DIR}/bash/*.sh

#install node modules
cd ${GIT_DIR} && npm i

#check if npm installation run succesfully by checking modules listed in package.json and compare them with npm ls
echo "Checking npm package installation..."
PACKAGE_JSON=${GIT_DIR}/package.json
PACKAGE_JSON_CONTENT=$(cat $PACKAGE_JSON)

devDependencies=$(echo $PACKAGE_JSON_CONTENT | sed -r 's/\ //g' | grep -Po '"devDependencies"([^}]+)' | sed -r 's/\"//g' | sed -r 's/devDependencies([^{]+)\{//g' | sed -r 's/[\,]/\n/g' | sed -r 's/\:([^\n]+)//g')
arrDevDependencies=(${devDependencies//\ / })

dependencies=$(echo $PACKAGE_JSON_CONTENT | sed -r 's/\ //g' | grep -Po '"dependencies"([^}]+)' | sed -r 's/\"//g' | sed -r 's/dependencies([^{]+)\{//g' | sed -r 's/[\,]/\n/g' | sed -r 's/\:([^\n]+)//g')
arrDependencies=(${dependencies//\ / })

arrBindings=("@serialport/bindings@" "@serialport/binding-mock@" "@serialport/binding-abstract@")

packages=("${arrDependencies[@]}" "${arrDevDependencies[@]}" "${arrBindings[@]}")

echo "Checking npm package installation..."
NPM_LIST=$(cd ${GIT_DIR} && npm ls)
for item in ${packages[@]}; do
	check=$(echo $NPM_LIST | grep -o "${item}@" | tail -1)
	if [[ -z $check ]]; then
		PACKAGE_NOT_INSTALLED=${item}
		break
	fi
done

#change git URL to https. Using '#' as delimiter because URLs contain '/' characters
sed -i "s#$GIT_SSH_URL#$GIT_HTTPS_URL#g" ${GIT_DIR}/.git/config

#remove ssh key
rm -rf ~/.ssh/id_rsa
rm -rf ~/.ssh/id_rsa.pub
rm -rf ~/.ssh/known_hosts

#~ clear

#copy config sample in order to get application running
#cp ${GIT_DIR}/config/app.config.sample.js ${GIT_DIR}/config/app.config.js
cp ${ASSETS_DIR}/app.config.js ${GIT_DIR}/config/app.config.js


#Set config
printf "Would you like to edit app.config.js "
read -t 10 -n 1 -p "(Y/n)?" editAppConfig

if [[ "$editAppConfig" = "y" || "$editAppConfig" = "Y" ]]; then
	vi ${GIT_DIR}/config/app.config.js
fi

#~ clear

#create autostart
printf "===== Creating autostart ====="

AUTOSTART_FILEPATH="/opt/start-dcs.sh"

printf "#!/bin/bash\n\
cd ${GIT_DIR} && npm start" | sudo tee ${AUTOSTART_FILEPATH}

sudo chown $USER:$USER ${AUTOSTART_FILEPATH}
sudo chmod 755 ${AUTOSTART_FILEPATH}

printf "[Desktop Entry]\n\
Type=Application\n\
Exec=${AUTOSTART_FILEPATH}\n\
Hidden=false\n\
NoDisplay=false\n\
X-GNOME-Autostart-enabled=true\n\
Name[en_US]=dcs-device\n\
Name=dcs-device\n\
Comment[en_US]=run dcs device\n\
Comment=run dcs device" | sudo tee /etc/xdg/autostart/sugity_dcs.desktop

#create desktop shortcut
printf "===== Creating desktop shortcut ====="


#set network
${DIR}/scripts/11-network.sh
ipAddress="$(cat ${LOGFILE} | grep "IP_ADD"| head -1 | sed -E "s/(IP_ADDRESS=\")|(\")//g")"

#create log for this installation, also used as a flag that this script has been run before
printf "DIR=\"${GIT_DIR}\"\nGIT_BRANCH=\"$GIT_BRANCH\"\nGIT_USER=\"$gitUser\"\nGIT_SSH_URL=\"$GIT_SSH_URL\"\nGIT_HTTPS_URL=\"$GIT_HTTPS_URL\"\nIP_ADDRESS=\"$ipAddress\"\n" | tee ${LOGFILE} > /dev/null

#clear bash history
history -c
rm -f ~/.bash_history
echo 'history -c' >> ~/.bashrc
echo 'set +o history' >> ~/.bashrc
echo 'history -c' >> ~/.bash_logout


#removing nopasswd
sudo sed -i '/NOPASSWD\:ALL/d' $sudoersFile

#~ clear

printf "Installation Finished.\n"
printf "IP Address : $ipAddress\n"
doReboot="y"
read -t 30 -p "Reboot in 30 Seconds..." doReboot
if [[ "$doReboot" == "y" || "$doReboot" == "Y" || -z $doReboot ]]; then
	rm -rf ${DIR}
	printf "%s\n" "$OSPassword" | sudo --stdin reboot
else
	printf "\nFinished\n"
	rm -rf ${DIR} && cd
fi
