module.exports = {
	socket : {
		path : '__TARGET_SOCKET_PATH__',
		port : __TARGET_SOCKET_PORT__,
		whitelist : [
			'127.0.0.1/31',
		],
	},

	machineId: __TARGET_MACHINE_ID__,

	rfid: {
			ip: "__TARGET_RFID_IP__",
			port: __TARGET_RFID_PORT__,
			id: __TARGET_RFID_ID__,
			timeout: __TARGET_RFID_TIMEOUT__,
	},

	plc: {
			ip: "__TARGET_PLC_IP__",
			channel: __TARGET_PLC_CHANNEL__,
			portUDP: __TARGET_PLC_PORTUDP__
	},

	window : {
		width : __TARGET_WINDOW_WIDTH__,
		height : __TARGET_WINDOW_HEIGHT__,
		fullscreen : __TARGET_WINDOW_FULLSCREEN__,
	},

	api: {
		baseurl: '__TARGET_API_BASE_URL__',
		username: '__TARGET_API_USERNAME__',
		password: '__TARGET_API_PASSWORD__',
		isRegistered: true,
	},

	autoreload : __TARGET_AUTORELOAD__,

	debug : false,
};
